TEMPLATE = app
CONFIG+= C++11

QT += qml quick widgets

SOURCES += main.cpp \
    qmlfunctorslot.cpp

RESOURCES += \
    Resources.qrc



# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

FORMS +=

HEADERS += \
    qmlfunctorslot.h

DISTFILES += \
    SimpleQml.qml

INCLUDEPATH += C:/BoostWinRtArm/include/boost-1_57
LIBS += -LC:/BoostWinRtArm/lib \
        -llibboost_chrono-vc120-mt-1_57 \
        -llibboost_thread-vc120-mt-1_57 \
        -llibboost_system-vc120-mt-1_57
