
#include "qmlfunctorslot.h"

#include <QApplication>
#include <QQmlApplicationEngine>

#include <boost/chrono/chrono.hpp>
#include <boost/date_time/posix_time/time_formatters_limited.hpp>
#include <iostream>

int main(int argc, char *argv[])
{

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/SimpleQml.qml")));

    QObject *rootObject = engine.rootObjects().first();

    QMLFunctorSlot sloter(&engine, [rootObject](){

        typedef boost::chrono::duration<long, boost::ratio<60> > minutes;

        // copied code from boost example
        // http://www.boost.org/doc/libs/1_57_0/doc/html/chrono/users_guide.html#chrono.users_guide.tutorial
        boost::chrono::nanoseconds start;
        boost::chrono::nanoseconds end;
        typedef boost::chrono::milliseconds ms;
        ms d = boost::chrono::duration_cast<ms>(end - start);


        QMetaObject::invokeMethod(rootObject, "updateMessage",
        Q_ARG(QVariant,  QString::fromStdString(boost::lexical_cast<std::string>( d.count()))));
    });

    QObject::connect(rootObject, SIGNAL(qmlSignal(QString)),
                     &sloter, SLOT(CallFunctor()));

    return app.exec();

}
