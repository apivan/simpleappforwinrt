#ifndef QMLFUNCTORSLOT_H
#define QMLFUNCTORSLOT_H

#include <QObject>
#include <functional>

class QMLFunctorSlot : public QObject
{
  Q_OBJECT

public:
   QMLFunctorSlot(QObject *parent, std::function<void()> function) :
       QObject(parent),m_function(function){}

  ~QMLFunctorSlot() = default;


public slots:
   void CallFunctor()
   {
       m_function();
   }

private :
   std::function<void()> m_function;

};


#endif // QMLFUNCTORSLOT_H
